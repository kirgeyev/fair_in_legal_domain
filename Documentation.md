Certainly! Here is the complete documentation for your metadata standard in Markdown format:

---

# US Cities Crime Metadata Schema Documentation

## Introduction

The US Cities Crime Metadata Schema is designed to provide a comprehensive and structured way to describe crime-related datasets. It uses standard namespaces and detailed metadata elements to facilitate better data management, sharing, and reuse.

## Namespaces Used

- **xmlns:xsi**: http://www.w3.org/2001/XMLSchema-instance
- **xmlns:xsd**: http://www.w3.org/2001/XMLSchema
- **xmlns:rdf**: http://www.w3.org/1999/02/22-rdf-syntax-ns#
- **xmlns:dcterms**: http://purl.org/dc/terms/
- **xmlns:foaf**: http://xmlns.com/foaf/0.1/
- **xmlns:ucr**: http://example.org/ontology/ucr#
- **xmlns:geo**: http://www.w3.org/2003/01/geo/wgs84_pos#
- **xmlns:time**: http://www.w3.org/2006/time#
- **xmlns:prov**: http://www.w3.org/ns/prov#

## Namespaces Explanation

#### RDF (Resource Description Framework)
- The RDF namespace (**rdf**) is used for defining resources and their relationships in a way that can be understood by machines. It is a standard model for data interchange on the web.
#### DCTERMS (Dublin Core Terms)
- The DCTERMS namespace (**dcterms**) is used for metadata elements and refinements to describe a wide range of resources. It is part of the Dublin Core Metadata Initiative.
#### FOAF (Friend of a Friend)
- The FOAF namespace (**foaf**) is used to describe people, their activities, and their relationships to other people and objects.
#### GEO (Geographical)
- The GEO namespace (**geo**) is used for describing geographical information based on the WGS84 standard.
#### TIME
- The TIME namespace (**time**) is used for describing temporal information.
#### PROV (Provenance)
- The PROV namespace (**prov**) is used for representing provenance information, which describes the origins, history, or lifecycle of data.
#### UCR (Uniform Crime Reporting)
- The UCR namespace (**ucr**) is used to define terms specific to crime reporting.
## Metadata Elements

### General

- **title**: The title of the dataset.
- **abstract**: A brief description of the dataset.
- **projectName**: The name of the project associated with the dataset.
- **metadataCreationDate**: The date when the metadata was created.
- **metadataLastModificationDate**: The date when the metadata was last modified.
- **dataLastModificationDate**: The date when the data was last modified.
- **doi**: The Digital Object Identifier for the dataset.
- **language**: The language of the dataset.
- **version**: The version of the metadata schema.
- **metadataSchema**: The name of the metadata schema being used.
- **funder**:
  - **funderName**: The name of the funder.
  - **funderIdentifier**: The identifier for the funder.

### Rights

- **dcterms:license**: Specifies the license under which the dataset is released.
- **dcterms:rightsHolder**: Identifies the entity holding the rights to the dataset.
- **dcterms:RightsStatement**: Provides a statement about the rights associated with the dataset.
- **dcterms:accessRights**: Describes the conditions or restrictions on access to the dataset.

### Contacts

- **metadataCreator**:
  - **foaf:name**: The name of the metadata creator.
  - **foaf:mbox**: The email address of the metadata creator.
- **dataCreator**:
  - **foaf:name**: The name of the data creator.
  - **foaf:mbox**: The email address of the data creator.
- **dataCollector**:
  - **foaf:name**: The name of the data collector.
  - **foaf:mbox**: The email address of the data collector.
- **contactPerson**:
  - **foaf:name**: The name of the contact person.
  - **foaf:mbox**: The email address of the contact person.
  - **foaf:phone**: The phone number of the contact person.
  - **foaf:address**: The address of the contact person.
  - **foaf:availability**: The availability of the contact person.

### Coverage

- **temporalCoverage**:
  - **time:hasBeginning**: The start date of the temporal coverage.
  - **time:hasEnd**: The end date of the temporal coverage.
- **spatialCoverage**:
  - **geo:location**: The spatial location covered by the dataset.

### Methods

- **dataCollection**:
  - **source**: The source of the data.
  - **description**: A description of how the data was collected.
  - **processDescription**: A description of the data processing methods.
- **studyDesign**: The design of the study from which the data was collected.
- **equipment**: The equipment used for data collection.

### Keywords

- **processesAndServices**: Keywords describing processes and services related to the data.
- **bioticDataTypes**: Keywords describing the types of biotic data.
- **habitats**: Keywords describing habitats related to the data.
- **relativePositionToGround**: Keywords describing the relative position to the ground.
- **additionalKeywords**:
  - **keyword**: Additional keywords related to the data.

### DataTypeAndStatus

- **dataProcessingStatus**: The status of data processing.
- **dataUploadStatus**: The status of data upload.
- **contentType**: The type of content in the dataset.
- **dataFormats**: The formats in which data is available (e.g., CSV, JSON, XML).

### DataAccess

- **accessURL**: The URL where the dataset can be accessed.
- **accessConditions**: The conditions for accessing the dataset.
- **authentication**: Specifies whether authentication is required to access the dataset.

### Provenance

- **prov:wasGeneratedBy**:
  - **prov:entity**: The entity that was generated.
  - **prov:activity**: The activity that generated the entity.
  - **prov:time**: The time when the entity was generated.
- **prov:wasDerivedFrom**:
  - **prov:generatedEntity**: The entity that was generated.
  - **prov:usedEntity**: The entity that was used in the derivation process.
  - **prov:type**: The type of derivation.
- **prov:wasAttributedTo**:
  - **prov:entity**: The entity that is attributed.
  - **prov:agent**: The agent responsible for the entity.
  - **prov:type**: The type of attribution.

### Ontologies

- **crimeOntology**:
  - **ucr:offenceCode**: The code representing a specific type of offense.
  - **ucr:offenceName**: The name of the offense.
- **geographicOntology**:
  - **geo:lat**: The latitude of a location.
  - **geo:long**: The longitude of a location.
  - **geo:location**: The location.
  - **geo:district**: The district or administrative division.
- **temporalOntology**:
  - **time:Instant**: A specific point in time.
  - **time:inXSDDateTime**: A date and time in XSD DateTime format.
  - **time:year**: The year component of a date.
  - **time:month**: The month component of a date.
  - **time:day**: The day component of a date.
  - **time:hour**: The hour component of a date.

### Attributes

- **attributes**: Additional attributes (optional).
